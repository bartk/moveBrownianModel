functions {
	vector lbRangeFun(matrix mm, vector y, real raise) {
		vector[rows(mm)] w; 
		real mmin;
		real mmax;
		vector[2] aa;
		if(cols(mm)==1)// in case of one column we can just return values directly
		{
			if(0>max(mm[,1]-raise)){
				return([negative_infinity(),0]);}
			if(0<=min(mm[,1]-raise))
			{
				return([0,positive_infinity()]);
			}
		}
		w=-((mm[, 2:cols(mm)] * y)./( mm[,1]-raise)); 
		mmin=1 ./min(1 ./w);
		mmax=1 ./max(1 ./w);
		if(0<=min(mm[,1]-raise))
		{
			if(is_inf(max(w)))
			{
				print("intern", y);
				return([negative_infinity(), positive_infinity()]);
			}

			return([max(w), positive_infinity()]);

		}
		if(0>max(mm[,1]-raise))
		{
			if(is_inf(min(w)))
			{
				print("Bug if this happens.");
				return([321, 123]);
			}
			return([negative_infinity(), min(w)]);
		}
		aa=[mmin, mmax]';
		if(0>min((mm[, 2:cols(mm)] * y)-raise*(mmin+(mmax-mmin)/2)+mm[,1]*(mmin+(mmax-mmin)/2)))
		{
			aa=[mmax, mmin]';
		}
		return(aa);
	}
}

data {
	int<lower=2> N;
	int<lower=1> NparMovement;
	int<lower=1> NparLocationError;
	matrix[N-1,NparMovement] movementMatrix; // covariance matrix of the movement 
	matrix[N,NparLocationError] locationErrorMatrix; // covariance matrix of the location error
	matrix[N,2] locationsMeters; // location matrix
	int<lower=1> nIndividuals;
	int<lower=1,upper=nIndividuals> locationIndividualLink[N];
	vector[N] timesSeconds;
	int nonNullMechanism;
	int<lower=0, upper=1> addOrigin;// one or 0 depending weather to include it in the estimations
}

transformed data {
	real<lower=0> movementMatrixRaise=0;
	real<lower=0> locationErrorMatrixRaise=0;
	int nSeg;
	vector[N-nIndividuals] timeDiffSeconds;
	vector[N-nIndividuals] zeroVec;
	int segmentsIndexes[N-nIndividuals];
	int segmentsIndexesPlusOne[N-nIndividuals];
	matrix[N-nIndividuals,2] locationDiffMeters;
	matrix[N-nIndividuals,NparMovement] movementMatrixSegments;
	matrix[N-nIndividuals,NparMovement] movementMatrixSegmentsRaised;
	matrix[N,NparLocationError] locationErrorMatrixRaised;
	real nSegReal;
	nSeg=N-nIndividuals;
	nSegReal= nSeg;
	zeroVec = rep_vector(0,nSeg);
	for(i in 1:(N-1)){
		if(!(((locationIndividualLink[i]-locationIndividualLink[i+1])==0) || 
					((locationIndividualLink[i]-locationIndividualLink[i+1])==-1)))
		{
			reject("individuals need to be numbered continuesly increasing")
		}
		if(locationIndividualLink[i]==locationIndividualLink[i+1]){
			timeDiffSeconds[i+1-locationIndividualLink[i]]= timesSeconds[i+1]-timesSeconds[i];
			locationDiffMeters[i+1-locationIndividualLink[i],1]=locationsMeters[i+1,1]-locationsMeters[i,1];
			locationDiffMeters[i+1-locationIndividualLink[i],2]=locationsMeters[i+1,2]-locationsMeters[i,2];
			segmentsIndexes[i+1-locationIndividualLink[i]]=i;
		}
	}
	// raise matrixes to deal with interaction
	movementMatrixSegments=movementMatrix[segmentsIndexes,];
	for(i in 1:nSeg){
		segmentsIndexesPlusOne[i]=segmentsIndexes[i]+1;
	}
	locationErrorMatrixRaised=locationErrorMatrix;
	movementMatrixSegmentsRaised=movementMatrixSegments;
	if(min(movementMatrixSegments[,1]) <= 0)// raise the first column so no negative values occur
	{
		movementMatrixRaise=fabs(min(movementMatrixSegments[,1]))+1;
	}
	if(min(locationErrorMatrix[,1]) <= 0)
	{
		locationErrorMatrixRaise=fabs(min(locationErrorMatrix[,1]))+1;
	}
	movementMatrixSegmentsRaised[,1]=movementMatrixSegments[,1]+movementMatrixRaise;
	locationErrorMatrixRaised[,1]=locationErrorMatrix[,1]+locationErrorMatrixRaise;
}
parameters {
	vector[NparMovement-1] suffixMovementPar;
	vector[NparLocationError-1] suffixLocationErrorPar;
	real<lower = lbRangeFun(movementMatrixSegmentsRaised, suffixMovementPar, movementMatrixRaise)[1]> firstMovementPar; // set limits to prevent negative values
	real<lower = lbRangeFun(locationErrorMatrixRaised, suffixLocationErrorPar, locationErrorMatrixRaise)[1]> firstLocationErrorPar;
	vector[addOrigin*2] originPar; // two dimensional
}
transformed parameters {
	vector[NparMovement] movementPar; 
	vector[NparLocationError] locationErrorPar;
	vector[nSeg] sigmaEstimatesSquare;
	vector[N] deltaEstimatesSquare;
	matrix[nSeg+addOrigin,2] fwdSolv;
	row_vector[2] fwdSolvTmp;
	vector[nSeg+addOrigin] ExDiag;
	vector[nSeg-1+addOrigin] ExOffDiag;
	vector[nSeg+addOrigin] CholDiag;
	vector[nSeg-1+addOrigin] CholOffDiag;
	if(nonNullMechanism==1){
		movementPar = append_row(firstMovementPar, suffixMovementPar);
		locationErrorPar =append_row(firstLocationErrorPar, suffixLocationErrorPar);
		sigmaEstimatesSquare= (movementMatrixSegmentsRaised * movementPar)- movementMatrixRaise* firstMovementPar ;
		deltaEstimatesSquare= (locationErrorMatrixRaised * locationErrorPar)- locationErrorMatrixRaise*firstLocationErrorPar;
		if(min(deltaEstimatesSquare)<0){
			reject("Negative delta^2 values, this should not occur");
		}
		if(min(sigmaEstimatesSquare)<0){
			reject("Negative sigma^2 values, this should not occur");
		}
	}else{
		if(nonNullMechanism==2){
			reject("Currently this is not supported");
			sigmaEstimatesSquare= exp(movementMatrixSegments * movementPar);
			deltaEstimatesSquare= exp(locationErrorMatrix * locationErrorPar);
		}
	}
	// slow inversion of E_x
	//  for(i in 1:(nSeg-1))
	//  {
	//     Ex[i,i+1]= -square(deltaEstimates[1]);
	//     Ex[i+1,i]= -square(deltaEstimates[1]);
	//  }
	//  Ex= diag_matrix(sigmaEstimates .* sigmaEstimates .* timeDiffSeconds+ 2* square(deltaEstimates[1]));
	//  Exinv= cholesky_decompose(Ex);
	// calculate diagonal of E_x
	ExDiag=append_row(rep_vector(deltaEstimatesSquare[1], addOrigin), sigmaEstimatesSquare .* timeDiffSeconds+
			deltaEstimatesSquare[segmentsIndexes]+deltaEstimatesSquare[segmentsIndexesPlusOne]);
	// calculate of diagonal of E_x
	ExOffDiag=
		-tail(deltaEstimatesSquare[segmentsIndexes], nSeg-1+addOrigin); // add origin here means first delta is added when needed need to check XX
	//for(i in 2:(nSeg)) XX check validity for add origin
	//{
	//  if((segmentsIndexes[i]-segmentsIndexes[i-1])!=1)
	//  ExOffDiag[i-1] =0;
	//}
	// make off diag 0 when not same individual
	// calculate cholesky factorization
	CholDiag[1]=sqrt(ExDiag[1]);
	for(i in 2:(nSeg+addOrigin))
	{
		CholOffDiag[i-1]=ExOffDiag[i-1]/CholDiag[i-1];
		CholDiag[i]=sqrt(ExDiag[i]-CholOffDiag[i-1]^2);
	}
	// similar to poz code forward solve
	if(addOrigin==0){
		fwdSolv[1,1]= locationDiffMeters[1,1]/CholDiag[1];
		fwdSolv[1,2]= locationDiffMeters[1,2]/CholDiag[1];
	}else{
		fwdSolv[1,1]= (locationsMeters[1,1]-originPar[1])/CholDiag[1];
		fwdSolv[1,2]= (locationsMeters[1,2]-originPar[2])/CholDiag[1];
	}
	fwdSolvTmp= fwdSolv[1,];
	for(i in 2:(nSeg+addOrigin)){
		fwdSolv[i,1]= (locationDiffMeters[i-addOrigin,1]-fwdSolvTmp[1]*CholOffDiag[i-1])/CholDiag[i];
		fwdSolv[i,2]= (locationDiffMeters[i-addOrigin,2]-fwdSolvTmp[2]*CholOffDiag[i-1])/CholDiag[i];
		fwdSolvTmp= fwdSolv[i,];
	}
}

model {
	target += -nSegReal/2 * log(2*pi()) - sum(log(CholDiag))
		- sum(fwdSolv[,1].*fwdSolv[,1])/2	+
		-nSegReal/2 * log(2*pi())
		- sum(log(CholDiag)) - sum(fwdSolv[,2].*fwdSolv[,2])/2;
}

// generated quantities {
// 	real fwdl;
// 	fwdl=
// 		-nSegReal/2 * log(2*pi()) - sum(log(CholDiag))
// 		- sum(fwdSolv[,1].*fwdSolv[,1])/2
// 		- sum(log(CholDiag)) - sum(fwdSolv[,2].*fwdSolv[,2])/2;
// 
// }
