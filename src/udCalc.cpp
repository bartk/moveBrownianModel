
#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

using namespace arma;

#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
arma::mat udCalc( // Function that calculates an intergated brownian bridge as a function of sigma values and mean locations
		NumericMatrix Ut, // 2 by number of locations matrix of the mean locations of normal distributsions
		NumericVector sigma2t, // the variance for each location
		NumericVector xGrid, // x and y locations of the grid cell centers
		NumericVector yGrid, 
		NumericVector ext)
{
	mat res(yGrid.size(),xGrid.size()); // the results matrix
	res.fill(0);
	const int n = sigma2t.size();
	double sigma2; // time for estimate
	double mux, muy;
	int segment =0;
	int xEnd, xStart, yEnd, yStart =0;
	NumericVector::iterator it;
	for (int ti=0; ti < n; ++ti) {
		sigma2=sigma2t[ti];
		//Rprintf("asdf %f\n",Ut(ti,0)); 

		if(xGrid[0]>=Ut(ti,0)-ext[0]*sqrt(sigma2))
		{
			stop(sprintf<300>("Lower x grid not large enough. Coordinates until %f given, until %f needed at least.",
						xGrid[0], 
						Ut(ti,0)-ext[0]*sqrt(sigma2) ));
		}
		if(xGrid[xGrid.size()-1]<=Ut(ti,0)+ext[0]*sqrt(sigma2))
		{      stop(sprintf<300>("Higher x grid not large enough. Coordinates until %f given, until %f needed at least.",
					xGrid[xGrid.size()-1], 
					Ut(ti,0)+ext[0]*sqrt(sigma2) ));
		}  
		if(yGrid[0]>=Ut(ti,1)-ext[0]*sqrt(sigma2))
		{
			stop(sprintf<300>("Lower y grid not large enough. Coordinates until %f given, until %f needed at least.",
						yGrid[0], 
						Ut(ti,1)-ext[0]*sqrt(sigma2) ));
		}
		if(yGrid[yGrid.size()-1]<=Ut(ti,1)+ext[0]*sqrt(sigma2))
		{      stop(sprintf<300>("Higher y grid not large enough. Coordinates until %f given, until %f needed at least.",
					yGrid[yGrid.size()-1], 
					Ut(ti,1)+ext[0]*sqrt(sigma2) ));
		}
	} 
	for (int ti=0; ti < n; ++ti) {
		sigma2=sigma2t[ti];

		it = xGrid.begin();
		while (*it <=Ut(ti,0)-ext[0]*sqrt(sigma2))
		{
			it++;
		}
		xStart=it-xGrid.begin()-1;
		//   it = xGrid.begin();
		while (*it <=Ut(ti,0)+ext[0]*sqrt(sigma2))
		{
			it++;
		}
		xEnd=it-xGrid.begin();
		it = yGrid.begin();
		while (*it <=Ut(ti,1)-ext[0]*sqrt(sigma2))
		{
			it++;
		}
		yStart=it-yGrid.begin()-1;
		while (*it <=Ut(ti,1)+ext[0]*sqrt(sigma2))
		{
			it++;
		}
		yEnd=it-yGrid.begin();

		NumericVector y=pow( yGrid[Rcpp::Range(yStart, yEnd)]-Ut(ti,1),2);
		NumericVector x=pow( xGrid[Rcpp::Range(xStart, xEnd)]-Ut(ti,0),2);
		arma::mat dists4 =mat(yEnd-yStart+1,xEnd-xStart+1,fill::zeros);
		dists4.each_col() += Rcpp::as<arma::vec>(y);
		dists4.each_row() += Rcpp::as<arma::rowvec>(x);

		arma::mat dists2= (1/((2*M_PI)*(sigma2)))*exp(-1*((dists4)/(2*sigma2)));

		res.submat(yStart,xStart, size(dists2))=dists2.submat(0, 0, size(dists2))+res.submat(yStart,xStart, size(dists2));
	}
	return(flipud(res));
}  
