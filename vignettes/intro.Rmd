---
title: "Introduction into moveBrownianModel"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Introduction into moveBrownianModel}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r include = FALSE}
knitr::opts_chunk$set(collapse = TRUE, comment = "#>")
```

First load the package, and some example data from the move package. This data has been collected by Scott LaPoint and collaborators on fishers using an acceleration informed gps device (https://www.datarepository.movebank.org/handle/10255/move.330)

```{r}
require(moveBrownianModel)
require(move)
filePath<-system.file("extdata","ricky.csv.gz",package="move")
ricky <- move(filePath)
```

Make a local projection

```{r}
data<-spTransform(ricky, center=T)
```

Lets have a quick look at the trajectory

```{r}
plot(data, type='l')
```

Now we can now fit a model plot the MCMC traces

```{r, }
set.seed(123)
nChain<-1
inits<-list(list(originPar=c(head(coordinates(data),1))), movementPar=c(10.6,3.5,.4,7.5,5.5,3), locationErrorPar=4)[rep(1,nChain)]
res<-BBCM(locationErrorFormula = ~ 1, movementFormula = ~periodicMovementSmoothMgcv(absorb.cons = T), 
          data=data, chains=nChain, cores=1, warmup=50, iter=75, init=inits)
rstan::stan_trace(res,inc_warmup = FALSE, 
                  pars = c('movementPar[1]','movementPar[2]','movementPar[3]','movementPar[4]',
                           'movementPar[5]','movementPar[6]','locationErrorPar[1]', 'originPar[1]',
                           'originPar[2]'))
rstan::stan_trace(res,inc_warmup = TRUE, 
                  pars = c('movementPar[1]','movementPar[2]','movementPar[3]','movementPar[4]',
                           'movementPar[5]','movementPar[6]','locationErrorPar[1]', 'originPar[1]', 
                           'originPar[2]'))
```
 
When we investigate the residuals we see that there is one very high deviating value, lets inspect this further and omit it.
 
```{r}
tail(sort(resid<-abs(getStandardizedResiduals(res, type='maxlikelihood'))))
which(rowMeans(resid> 10)!=0)
plot(a<-data[8585:8589,], type='l')
speed(a)
timeLag(a,units='mins')
distance(a)
dataHighSpeed<-data[-8588,]
```
Now run the model again

```{r}
res2<-BBCM(locationErrorFormula = ~ 1, movementFormula = ~periodicMovementSmoothMgcv(absorb.cons = T), 
           data=dataHighSpeed, chains=nChain, 
           cores=1, warmup=50, iter=75, init=inits)
rstan::stan_trace(res2,inc_warmup = FALSE, 
                  pars = c('movementPar[1]','movementPar[2]','movementPar[3]','movementPar[4]',
                           'movementPar[5]','movementPar[6]','locationErrorPar[1]', 'originPar[1]', 
                           'originPar[2]'))
rstan::stan_trace(res2,inc_warmup = TRUE, 
                  pars = c('movementPar[1]','movementPar[2]','movementPar[3]','movementPar[4]',
                           'movementPar[5]','movementPar[6]','locationErrorPar[1]', 'originPar[1]', 
                           'originPar[2]'))
```
The residuals are still not very good and require further investigation but are much better then before. Most likely integrating the knowledge of the acceleration used to inform the GPS schedule will help to better understand the structure of the trajectory. 
```{r}
hist(getStandardizedResiduals(res2, type='maxlikelihood'))
```


