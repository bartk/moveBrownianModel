[![](http://www.r-pkg.org/badges/version/moveBrownianModel)](http://cran.rstudio.com/web/packages/moveBrownianModel/index.html)
[![build status](https://gitlab.com/bartk/moveBrownianModel/badges/master/pipeline.svg)](https://gitlab.com/bartk/moveBrownianModel/pipelines)

### Install the development version

``` R
require('devtools')
install_git('https://gitlab.com/bartk/moveBrownianModel.git')
```

As it is still a package in development any feedback on both instalation and usage is welcome.

### Documentation

Some documentation can be found at the [website](https://bartk.gitlab.io/moveBrownianModel/).
