% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/getPar.R
\name{getStandardizedResiduals}
\alias{getStandardizedResiduals}
\title{Get the standardized residuals}
\usage{
getStandardizedResiduals(object, ...)
}
\arguments{
\item{object}{The result of fitting a BBCM model using the \code{\link{BBCM}}.}

\item{...}{parameters passed on to \code{\link{getPar}}.}
}
\value{
A vector with n-1 elements, where N is the number of observed locations
}
\description{
Get the standardized residuals
}
\seealso{
getPar
}
