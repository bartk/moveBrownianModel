# ' @useDynLib browMod, .registration=TRUE
#' @import Rcpp
#' @import methods
#' @import move
#' @importFrom sp coordinates
#' @importFrom stats model.matrix
#' @importFrom rstan sampling
#' @importFrom mgcv cSplineDes
#' @importFrom utils head tail


#' @importFrom lubridate floor_date ceiling_date as.period interval
NULL

#' @title Function to fit a model to a movement trajectory
#'
#' @param movementFormula Formula specifying the movement model
#' @param locationErrorFormula  Formula specifying the formula for the location error
#' @param data a move object
#' @param pars parameters to store
#' @param nonNullMechanism option for possible other non null mechanism, currently only limits are used
#' @param addOrigin A logical indicating whether to add the origin to the likelyhood estimation
#' @param init a list of initalization values
#' @param ... arguments passed on to \code{\link[rstan:sampling]{rstan::sampling}}, for example:
#' \describe{
#'   \item{chains}{the number of chains}
#'   \item{cores}{the number of cores to use}
#'   \item{warmup}{the number of warmup iterations}
#'   \item{iter}{the total number of iterations}
#' }
#'
#' @description Tools used for fitting a model
#' @export
#' 
#' @examples
#' set.seed(234525)
#' t <- round(rgamma(4000, 3, .37)) + 1
#' d <- data.frame(
#'     cont = runif(length(t)),
#'     fac = sample(
#'       factor(LETTERS[1:2]),
#'       size = length(t),
#'       replace = TRUE,
#'       prob = c(5, 3)
#'     ),
#'     err = sample(
#'       factor(LETTERS[1:3]),
#'       size = length(t),
#'       replace = TRUE,
#'       prob = c(1, 2, 1)
#'     )  )
#' s <- .5 * d$cont + c(.3, 1.8)[d$fac]
#' pos <- apply(rbind(c(0, 0),  
#'              replicate(2,
#'                        unlist(
#'                          lapply(mapply(rnorm, t, sd = sqrt(s)), sum)
#'                          ))), 2, cumsum)[1:nrow(d), ] + 
#'                          replicate(2, rnorm(nrow(d), sd = sqrt(c(1,1.3,3)[d$err])))
#' data <-
#'   move(pos[, 1]+500, pos[, 2]+110, Sys.time() + cumsum(c(0, head(t,-1))), data = d)
#' res <-
#'   BBCM(locationErrorFormula = ~ err,
#'     movementFormula = ~ fac + cont,
#'     data = data,
#'     iter = 500,
#'     chains = 2,cores=1,
#'     init=function(chain_id){list(locationErrorPar=c(.5,0,0),
#'     movementPar=c(1,0,0,0), originPar=c(500,100))}
#'   )
#' \dontrun{
#' require(rstan)
#' stan_trace(res, inc_warmup=TRUE)
#' require(shinystan)
#' launch_shinystan(res)
#' }
#' set.seed(234525)
#' n<-1000
#' t <- round(rgamma(n, 3, .37)) + 1
#' d<-runif(n,1,2)*.6
#' pos<-replicate(2,cumsum(c(0,rnorm(n,sd=sqrt(d*t)))))
#' le<-c(a=.4,b=.8)
#' e<-sample(size=n+1, letters[1:2], prob = 2:1, replace = TRUE)
#' rpos<-pos+cbind(rnorm(1+n,sd=sqrt(le)[e]),rnorm(1+n,sd=sqrt(le)[e]))
#' tt<-Sys.time()
#' data <-
#'   move(rpos[, 1], rpos[, 2],Sys.time()+cumsum(c(0,t)) , 
#'        data = data.frame(err=e, cont=c(d,0)))
#' res <-
#'  BBCM(locationErrorFormula = ~ err,
#'                 movementFormula = ~0+  cont,
#'                 data = data,
#'                 iter = 500,
#'                 chains = 2,
#'                 init=function(chain_id){list(
#'                   locationErrorPar=c(.5,0),
#'                   movementPar=array(c(1),1))}
#'   )




BBCM <-
  function(movementFormula =  ~ 1,
           locationErrorFormula =  ~ 1,
           data,
           pars = c(
             "movementPar",
             "locationErrorPar",'originPar', 'ExDiag'
           ),
           nonNullMechanism = c("samplelimits", "exp"), addOrigin=T,
           init='random',
           ...) {
    stopifnot(is.logical(addOrigin))
    stopifnot(length(addOrigin)==1)
    
        mm <-
      dataToModelMatrix(
        data,
        movementFormula = movementFormula,
        locationErrorFormula = locationErrorFormula
      )
    nonNullInt <-
      c(samplelimits = 1, exp = 2)[match.arg(nonNullMechanism)]
    if(n.indiv(data)==1){
      indivIds<-rep(1L,n.locs(data))
    }else{
      indivIds<-as.integer(trackId(data))
    }
    stopifnot(all(table(indivIds)>1))
    if(is.list(init)){
      for(i in 1:length(init))
      {
        if('movementPar' %in% names(init[[i]])){
          init[[i]]$firstMovementPar<-init[[i]]$movementPar[1]
          init[[i]]$suffixMovementPar<-init[[i]]$movementPar[-1]
          init[[i]]$movementPar<-NULL
        }
        if('locationErrorPar' %in% names(init[[i]])){
          init[[i]]$firstLocationErrorPar<-init[[i]]$locationErrorPar[1]
          init[[i]]$suffixLocationErrorPar<-init[[i]]$locationErrorPar[-1]
          init[[i]]$locationErrorPar<-NULL
        }
      }
    }
    if(TRUE){
      movementMatrixColMeans<-colMeans(mm$movement)[-1]
      movementMatrixRange<-diff(apply(mm$movement, 2, range))[-1]
      locationErrorMatrixColMeans<-colMeans(mm$locationError)[-1]
      locationErrorMatrixRange<-diff(apply(mm$locationError, 2, range))[-1]
      mm$movement[,-1]<-t((t(mm$movement[,-1])-movementMatrixColMeans)/movementMatrixRange)
      mm$locationError[,-1]<-t((t(mm$locationError[,-1])-locationErrorMatrixColMeans)/locationErrorMatrixRange)
    }
    res <- sampling(
      stanmodels[["indiv"]],
      data = list(
        N = sum(n.locs(data)),
        NparMovement = dim(mm[["movement"]])[2],
        NparLocationError = dim(mm[["locationError"]])[2],
        movementMatrix = mm[["movement"]],
        locationErrorMatrix = mm[["locationError"]],
        locationsMeters = coordinates(data),
        nIndividuals= n.indiv(data),
        locationIndividualLink=indivIds,
        timesSeconds = as.numeric(timestamps(data), units='secs'),
        nonNullMechanism = nonNullInt, addOrigin=as.numeric(addOrigin)
      ),
      pars = pars,init=init,
      ...
    )
    ret<-new(
      paste0("BBCM",sub(x=class(data),'Move','')),
      locationErrorFormula = locationErrorFormula,
      movementFormula = movementFormula,modelMatrix=mm,
      nonNullMechanism = nonNullMechanism,
      data = data,
      res
    )
    attr(ret,'normalization')<- list(
      movementMatrixColMeans=movementMatrixColMeans,
      movementMatrixRange=movementMatrixRange,
      locationErrorMatrixColMeans=locationErrorMatrixColMeans,
      locationErrorMatrixRange=locationErrorMatrixRange
    )
    return(
     ret 
    )
  }

#' @importFrom rstan optimizing extract
#'
NULL

#' @rdname BBCM
BBCMMaxLikelihood <-
  function(movementFormula =  ~ 1,
           locationErrorFormula =  ~ 1,
           data,
           nonNullMechanism = c("reject", "exp"),addOrigin=T,
           ...) {
    stopifnot(is.logical(addOrigin))
    stopifnot(length(addOrigin)==1)
    mm <-
      dataToModelMatrix(
        data,
        movementFormula = movementFormula,
        locationErrorFormula = locationErrorFormula
      )
    nonNullInt <-
      c(reject = 1, exp = 2)[match.arg(nonNullMechanism)]
    if(n.indiv(data)==1){
      indivIds<-rep(1L,n.locs(data))
    }else{
      indivIds<-as.integer(trackId(data))
    }
    res <- optimizing(
      stanmodels[["indiv"]],
      data = list(
        N = sum(n.locs(data)),
        NparMovement = dim(mm[["movement"]])[2],
        NparLocationError = dim(mm[["locationError"]])[2],
        movementMatrix = mm[["movement"]],
        locationErrorMatrix = mm[["locationError"]],
        locationsMeters = coordinates(data),
        nIndividuals= n.indiv(data),
        locationIndividualLink=indivIds,
        timesSeconds = as.numeric(timestamps(data), units='secs'),
        nonNullMechanism = nonNullInt, addOrigin=as.numeric(addOrigin)
      ),# todo XX move tthis data function to seperate function
      ...
    )
    attr(res,'data')<-data
    attr(res,'locationErrorFormula')<-locationErrorFormula
    attr(res,'movementFormula')<-movementFormula
    attr(res,'modelMatrix')<-mm
    return(res)
  }


plotControl<- function(res,...){
  std<-getStandardizedResiduals(res,...)
  ggplot(data=data.frame(x=c(std)))+
  geom_histogram( aes_string(x="x",y="..density.."))+
  stat_function(fun=dnorm, color='red', args = list(mean=0, sd=1))
}

plotControlDiag<- function(res,...){
  std<-getStandardizedResiduals(res,...)
  ExDiag<-getPar(res,par='ExDiag',...)
  d<-data.frame(x=c(std),xx=sqrt(c(head(ExDiag,-1),head(ExDiag,-1))))
  ggplot(data=d)+
    geom_point(aes_string(x='xx',y='x'))
}

plotControlQQ<- function(res,...){
  std<-getStandardizedResiduals(res,...)
  ggplot(aes_string(sample='std'), data=data.frame(std=c(std)))+
    stat_qq(dparams=list(mean=0, sd=1))+
    geom_abline(slope = 1, intercept = 0, colour='red')
}

#' expands stanfit to include model fit
#'
#' @slot movementFormula formula.
#' @slot locationErrorFormula formula.
#' @slot nonNullMechanism character.
#' @slot data Move.
#' @slot modelMatrix the design matrix used for the model
#'
#' @export
setClass(
  "BBCM",
  representation = representation(
    movementFormula = "formula",
    locationErrorFormula = "formula",
    nonNullMechanism = "character",
    modelMatrix= 'list',
    
    data = "Move"
  ),
  contains = "stanfit"
)

#' expands stanfit to include model fit
#'
#' @slot movementFormula formula.
#' @slot locationErrorFormula formula.
#' @slot nonNullMechanism character.
#' @slot data MoveStack.
#' @slot modelMatrix  the design matrix used for the mode
#'
#' @export
setClass(
  "BBCMStack",
  representation = representation(
    movementFormula = "formula",
    locationErrorFormula = "formula",
    nonNullMechanism = "character",
    modelMatrix= 'list',
    data = "MoveStack"
  ),
  contains = "stanfit"
)

#' @importFrom stats predict
#' @importFrom rstan extract
#'
NULL

#' if(FALSE){
#'   
#' if (!isGeneric("predict")) {
#'   setGeneric("predict", function(object, ...)
#'     standardGeneric("predict"))
#' }
#' #' XXasdfwe
#' #' 
#' #' @param object sadfXX
#' #' @param newdata asdfXX
#' #' @param type wrXX
#' #' @param ... nothing implemented yet
#' #' 
#' #' @return asdfXX
#' #' @export
#' #' @method predict BBCM
#' #' 
#' #' @examples
#' #' XX<-1
#' setMethod(
#'   "predict", signature(object = "BBCM"),
#'   # predict.BBCM <-
#'   function(object, newdata, type = "parameters", ...) {
#'     if (type == "parameters") {
#'       movementMatrix <-
#'         model.matrix(
#'           object@movementFormula,
#'           data = as.data.frame(newdata)
#'         )[-n.locs(newdata), , drop = FALSE]
#'       locationErrorMatrix <-
#'         model.matrix(
#'           object@locationErrorFormula,
#'           data = as.data.frame(newdata)
#'         )
#'       m <- which.max(extract(object, "lp__")[["lp__"]])
#'       locationErrorPar <-
#'         extract(object, "locationErrorPar")[["locationErrorPar"]][m, ]
#'       movementPar <-
#'         extract(object, "movementPar")[["movementPar"]][m, ]
#'       res <- data.frame(
#'         sigma = (c(movementMatrix %*% movementPar, NA)),
#'         delta = (locationErrorMatrix %*% locationErrorPar)
#'       )
#'       if (object@nonNullMechanism == "exp") {
#'         res <- exp(res)
#'       }
#'       return(res)
#'     }
#'   }
#' )
#' }
